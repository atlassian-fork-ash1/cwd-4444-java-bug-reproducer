import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Broken extends SSLSocketFactory
{
    SSLSocketFactory sf;

    public Broken() throws NoSuchAlgorithmException
    {
        sf = SSLContext.getDefault().getSocketFactory();
    }

    public static SSLSocketFactory getDefault()
    {
        try
        {
            return new Broken();
        } catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String[] getDefaultCipherSuites()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String[] getSupportedCipherSuites()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket(Socket socket, String s, int i, boolean b) throws IOException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket(String s, int i) throws IOException, UnknownHostException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket(String s, int i, InetAddress inetAddress, int i1) throws IOException, UnknownHostException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int i) throws IOException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress1, int i1) throws IOException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Socket createSocket() throws IOException {
        SSLSocket s = (SSLSocket) sf.createSocket();
        SSLParameters param = s.getSSLParameters();
        param.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(param);
        return s;
    }
}
