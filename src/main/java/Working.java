import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Working
{
    SSLSocketFactory sf;

    public Working() throws NoSuchAlgorithmException
    {
        sf = SSLContext.getDefault().getSocketFactory();
    }

    public static Object getDefault()
    {
        try
        {
            return new Working();
        } catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }

    public Socket createSocket(String host, int port) throws IOException, UnknownHostException
    {
        SSLSocket s = (SSLSocket) sf.createSocket(host, port);
        SSLParameters param = s.getSSLParameters();
        param.setEndpointIdentificationAlgorithm("LDAPS");
        s.setSSLParameters(param);
        return s;
    }
}
