package test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

import sun.misc.JavaNetAccess;
import sun.misc.SharedSecrets;

import static org.junit.Assert.assertEquals;

public class TestHostnameRetrieval
{
    // This should be a resolvable hostname that won't match reverse DNS lookup
    // from the IP address it resolves to.
    //
    // e.g. "localhost.localdomain" -> "127.0.0.1", and "127.0.0.1" -> "localhost"
    private static final String resolvableHostname = "localhost.localdomain";

    @Test
    public void inetAddressReturnsHostnameFromConstruction() throws UnknownHostException
    {
        InetAddress inetAddress = InetAddress.getByName(resolvableHostname);
        assertEquals(resolvableHostname, inetAddress.getHostName());
    }

    @Test
    public void sunJavaNetAccessCanGetOriginalHostNameAsHostName() throws UnknownHostException
    {
        JavaNetAccess jna = SharedSecrets.getJavaNetAccess();
        InetAddress inetAddress = InetAddress.getByName(resolvableHostname);
        assertEquals(resolvableHostname, jna.getOriginalHostName(inetAddress));
    }
}
